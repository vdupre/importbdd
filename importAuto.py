import sqlite3
import logging
import csv
import argparse
from sqlSIV import *
from datetime import datetime

def SIVExist(cursor):
    """
    Teste si la table SIV existe et la créée dans le cas contraire
    :param cursor: objet cursor de la BDD
    :type cursor: sqlite3.cursor object
    :return: Vrai si la table existent, faux sinon
    :rtype: bool
    """
    try:
        cursor.execute("SELECT * FROM SIV")
        return True
    except:
        cursor.executescript(createSIV)
        logging.info("Table SIV créée")
        return False

def readFile(file, delimiter):
    """
    Lit le fichier passé en paramètre.

    :param file: lien du fichier
    :type file: str
    :param delimiter: séparateur entre les champs
    :type delimiter: str
    :return: liste de dictionnaire
    :rtype: list
    """
    liste = []
    with open(file) as csvfile:
        reader = csv.DictReader(csvfile, delimiter=delimiter)
        for row in reader:
            liste.append(row)
    return liste



def dataExists(immat, cursor):
    """
    Teste si les données existent dans la BDD
    :param immat: immatriculation à tester
    :type data: str
    :param cursor: objet cursor de la BDD
    :type cursor: sqlite3.cursor object
    :return: Vrai si les données existent, faux sinon
    :rtype: bool
    """
    cursor.execute("select COUNT(*) from SIV where immatriculation = ?", immat)
    if (cursor.fetchone() == (0,)):
        return False
    else:
        return True

def insertData(data,cursor):
    """
    Insère les données dans la BDD
    :param data: données à insérer
    :type data: array
    :param cursor: objet cursor de la BDD
    :type cursor: sqlite3.cursor object
    :return: none
    :rtype: none
    """
    try:
        cursor.execute(insertSIV, data)
    except sqlite3.Error:
        logging.error('Insert error')
    


def updateData(data, cursor):
    """
    Met à jour les données dans la BDD
    :param data: données à mettre à jour
    :type data: array
    :param cursor: objet cursor de la BDD
    :type cursor: sqlite3.cursor object
    :return: none
    :rtype: none
    """
    try:
        cursor.execute(updateSIV, data)
    except sqlite3.Error:
        logging.error('update error')
        
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        "Mise à jour de la base de données avec les données du fichier CSV fourni")
    parser.add_argument("data_file", help="Le fichier de données.")
    args = parser.parse_args()

    logging.basicConfig(filename="logs/" + datetime.today().strftime('%Y-%m-%d') + ".log" , level=logging.DEBUG)

    connection = sqlite3.connect('AWL.sqlite3')
    logging.info('Connexion à la base de données')
    cursor = connection.cursor()

    datas = readFile(args.data_file, ';')
    SIVExist(cursor)


    for data in datas:
        immat = (data['immatriculation'],)
        if (dataExists(immat, cursor)):
            updateData(data, cursor)
        else:
            insertData(data, cursor)
    logging.info("Nombre de lignes modifiées : {}".format(connection.total_changes))
    connection.commit()
    connection.close()