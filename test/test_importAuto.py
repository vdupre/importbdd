import unittest
import csv
import os
from importAuto import SIVExist, dataExists, insertData, updateData
from sqlSIV import *
import sqlite3

class TestImportAuto(unittest.TestCase):

    def setUp(self):
        self.connection = sqlite3.connect('test/test.sqlite3')
        self.cursor = self.connection.cursor()
        self.data = dict([('address_titulaire', '982 Sanders Rapid Apt. 814 Jameschester, CO 06705'), ('nom', 'Robles'), ('prenom', 'Diana'), ('immatriculation', '0-87577'), ('date_immatriculation', '30/05/1935'), ('vin', '9781238079037'), ('marque', 'Miller-Brown'), ('denomination_commerciale', 'Proactive impactful paradigm'), ('couleur', 'Lavender'), ('carrosserie', '88-8825014'), ('categorie', '27-6194173'), ('cylindree', '3344'), ('energie', '16591493'), ('places', '44'), ('poids', '6056'), ('puissance', '234'), ('type', 'LLC'), ('variante', '62-3652072'), ('version', '00275590')])

    def tearDown(self):
        self.connection.close()
        os.remove('test/test.sqlite3')

    def test_SIVExist(self):
        self.assertEqual(SIVExist(self.connection), False) #La table n'existe pas et est donc créé
        self.assertEqual(SIVExist(self.connection), True) #La table existe
        
    def test_insertData(self):
        SIVExist(self.connection)
        self.cursor.execute("Select COUNT(*) from SIV")
        if (self.cursor.fetchone() == (0,)):
            insertData(self.data, self.cursor)
            self.cursor.execute("Select COUNT(*) from SIV")
            result = self.cursor.fetchone()[0]
            self.assertEqual(result,1)
            self.cursor.execute("Select * from SIV")
            result = self.cursor.fetchone()[0]
            self.assertEqual(result,self.data['address_titulaire'])

    def test_updateData(self):
        SIVExist(self.connection)
        self.cursor.execute("Select COUNT(*) from SIV")
        if (self.cursor.fetchone() == (0,)):
            insertData(self.data, self.cursor)
            self.cursor.execute("Select COUNT(*) from SIV")
            if (self.cursor.fetchone()[0] == 1):
                self.data['address_titulaire'] = "91 rue de Cottenchy"
                updateData(self.data, self.cursor)
                self.cursor.execute("Select * from SIV")
                result = self.cursor.fetchone()[0]
                self.assertEqual(result,self.data['address_titulaire'])
