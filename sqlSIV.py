createSIV = """create table SIV (
    address_titulaire TEXT not null, 
    nom TEXT not null,
    prenom TEXT not null,
    immatriculation TEXT primary key,
    date_immatriculation TEXT not null,
    vin INTEGER not null,
    marque TEXT not null,
    denomination_commerciale TEXT not null,
    couleur TEXT not null,
    carrosserie TEXT not null,
    categorie  TEXT not null,
    cylindree INTEGER not null,
    energie  INT not null,
    places INTEGER not null,
    poids NUMERIC not null,
    puissance INTEGER not null,
    type TEXT not null,
    variante TEXT not null,
    version INT not null
);"""

updateSIV = """UPDATE SIV SET address_titulaire = :address_titulaire, nom = :nom, prenom = :prenom,
    immatriculation = :immatriculation, date_immatriculation = :date_immatriculation, vin = :vin,
    marque = :marque, denomination_commerciale = :denomination_commerciale, couleur = :couleur,
    carrosserie = :carrosserie, categorie = :categorie, cylindree = :cylindree, 
    energie = :energie,  places = :places, poids = :poids,
    puissance = :puissance, type = :type, variante = :variante, version = :version 
    where immatriculation = :immatriculation ;
    """

insertSIV ="""INSERT into SIV
    values (:address_titulaire, :nom, :prenom,
    :immatriculation, :date_immatriculation, :vin,
    :marque, :denomination_commerciale, :couleur,
    :carrosserie, :categorie, :cylindree, 
    :energie, :places, :poids,
    :puissance, :type, :variante, :version )
    """